<?php

namespace App\Http\Controllers\Admin;

use App\Model\user\bankname;
use App\Model\user\cash;
use Illuminate\Http\Request;
use App\Model\user\account;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $accounts   = account::all();
        return view('admin.account.show',compact('accounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $banknames = bankname::all();
        $cashs = cash::all();
        return view('admin.account.account',compact('banknames','cashs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request,[

            'slug'=>'required',
            'advance'=>'required',
            'dues'=>'required',
            'cheque'=>'required',
            'total'=>'required',

        ]);

        $account = new account;
        $account->slug = $request->slug;
        $account->advance= $request->advance;
        $account->dues = $request->dues;
        $account->cheque = $request->cheque;
        $account->total = $request->total;
        $account->save();
        $account->banknames()->sync($request->bankname);
        $account->cashs()->sync($request->cash);

        return redirect(route('account.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $account = account::with('banknames','cashs')->where('id',$id)->first();
        $banknames = bankname::all();
        $cashs = cash::all();
        return view('admin.account.edit',compact('banknames','cashs','account'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request,[


            'slug'=>'required',
            'advance'=>'required',
            'dues'=>'required',
            'cheque'=>'required',
            'total'=>'required',
        ]);
        $account = account::find($id);
        $account->slug = $request->slug;
        $account->advance= $request->advance;
        $account->dues = $request->dues;
        $account->cheque = $request->cheque;
        $account->total = $request->total;
        $account->banknames()->sync($request->bankname);
        $account->cashs()->sync($request->cash);
        $account->save();

        return redirect(route('account.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        account::where('id',$id)->delete();
        return redirect()->back();
    }
}
