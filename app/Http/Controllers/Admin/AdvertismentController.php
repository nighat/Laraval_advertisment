<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\user\advertisment ;
use App\Http\Controllers\Controller;

class AdvertismentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {

        $advertisments = advertisment::all();
        return view('admin.advertisment.show', compact('advertisments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.advertisment.advertisment');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'name' => 'required',
            'org' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'refer' => 'required',
            'slide' => 'required',
            'duration' => 'required',
            'renew' => 'required',
            'date' => 'required',
            'expense' => 'required',
            'type' => 'required',
            'amount' => 'required',
            'comment' => 'required',

        ]);

        $advertisment = new advertisment;
        $advertisment->name = $request->name;
        $advertisment->org = $request->org;
        $advertisment->address = $request->address;
        $advertisment->phone = $request->phone;
        $advertisment->email = $request->email;
        $advertisment->refer = $request->refer;
        $advertisment->slide = $request->slide;
        $advertisment->duration = $request->duration;
        $advertisment->renew = $request->renew;
        $advertisment->date = $request->date;
        $advertisment->expense = $request->expense;
        $advertisment->type = $request->type;
        $advertisment->amount = $request->amount;
        $advertisment->comment = $request->comment;
        $advertisment->save();

        return redirect(route('advertisment.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.advertisment.view', ['advertisment' => advertisment::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $advertisment = advertisment::where('id', $id)->first();
        return view('admin.advertisment.edit', compact('advertisment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

            'name' => 'required',
            'org' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'refer' => 'required',
            'slide' => 'required',
            'duration' => 'required',
            'renew' => 'required',
            'date' => 'required',
            'expense' => 'required',
            'type' => 'required',
            'amount' => 'required',
            'comment' => 'required',

        ]);

        $advertisment = advertisment::find($id);
        $advertisment->name = $request->name;
        $advertisment->org = $request->org;
        $advertisment->address = $request->address;
        $advertisment->phone = $request->phone;
        $advertisment->email = $request->email;
        $advertisment->refer = $request->refer;
        $advertisment->slide = $request->slide;
        $advertisment->duration = $request->duration;
        $advertisment->renew = $request->renew;
        $advertisment->date = $request->date;
        $advertisment->expense = $request->expense;
        $advertisment->type = $request->type;
        $advertisment->amount = $request->amount;
        $advertisment->comment = $request->comment;
        $advertisment->save();

        return redirect(route('advertisment.index'))->with('message', 'Advertisment Updated Succecfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        advertisment::where('id', $id)->delete();
        return redirect()->back()->with('message', 'Advertisment Deleted Succecfully');
    }
}
