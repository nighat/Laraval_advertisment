<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\user\appoinment;
use App\Http\Controllers\Controller;

class AppoinmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {

        $appoinments = appoinment::all();
        return view('admin.appoinment.show',compact('appoinments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.appoinment.appoinment');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

            'name'=>'required',
            'address'=>'required',
            'phone'=>'required',
            'visit'=>'required',
            'date'=>'required',
            'time'=>'required',
            'purpose'=>'required',

        ]);

        $appoinment = new appoinment;
        $appoinment->name = $request->name;
        $appoinment->address = $request->address;
        $appoinment->phone = $request->phone ;
        $appoinment->visit = $request->visit ;
        $appoinment->date = $request->date ;
        $appoinment->time = $request->time ;
        $appoinment->purpose = $request->purpose;
        $appoinment->save();

        return redirect(route('appoinment.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.appoinment.view', ['appoinment' => appoinment::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $appoinment= appoinment::where('id',$id)->first();
        return view('admin.appoinment.edit',compact('appoinment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[

            'name'=>'required',
            'address'=>'required',
            'phone'=>'required',
            'visit'=>'required',
            'date'=>'required',
            'time'=>'required',
            'purpose'=>'required',

        ]);

        $appoinment = appoinment::find($id);
        $appoinment->name = $request->name;
        $appoinment->address = $request->address;
        $appoinment->phone = $request->phone ;
        $appoinment->visit = $request->visit ;
        $appoinment->date = $request->date ;
        $appoinment->time = $request->time ;
        $appoinment->purpose = $request->purpose;
        $appoinment->save();

        return redirect(route('appoinment.index'))->with('message','Appoinment Updated Succecfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        appoinment::where('id',$id)->delete();
        return redirect()->back()->with('message','Appoinment Deleted Succecfully');
    }
}
