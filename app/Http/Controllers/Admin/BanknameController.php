<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\user\bankname;
use App\Http\Controllers\Controller;

class BanknameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {

        $banknames = bankname::all();
        return view('admin.bankname.show',compact('banknames'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.bankname.bankname');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

            'name'=>'required',
            'brance'=>'required',
            'slug'=>'required',

        ]);

        $bankname = new bankname;
        $bankname->name = $request->name;
        $bankname->brance = $request->brance;
        $bankname->slug = $request->slug;
        $bankname->save();

        return redirect(route('bankname.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bankname= bankname::where('id',$id)->first();
        return view('admin.bankname.edit',compact('bankname'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[

            'name'=>'required',
            'brance'=>'required',
            'slug'=>'required',

        ]);

        $bankname = bankname::find($id);
        $bankname->name = $request->name;
        $bankname->brance = $request->brance;
        $bankname->slug = $request->slug;
        $bankname->save();

        return redirect(route('bankname.index'))->with('message','Bankname Updated Succecfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        bankname::where('id',$id)->delete();
        return redirect()->back()->with('message','Bankname Deleted Succecfully');
    }
}
