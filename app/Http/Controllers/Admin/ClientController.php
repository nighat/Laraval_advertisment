<?php

namespace App\Http\Controllers\Admin;

use App\Model\user\desingation;
use App\Model\user\item;
use Illuminate\Http\Request;
use App\Model\user\client;
use App\Http\Controllers\Controller;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $clients   = client::all();
        return view('admin.client.show',compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $desingations = desingation::all();
        $items = item::all();
        return view('admin.client.client',compact('desingations','items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request,[
            'name'=>'required',
            'slug'=>'required',
            'address'=>'required',
            'phone'=>'required',
            'webaddress'=>'required',
            'requirement'=>'required',
            'startdate'=>'required',
            'renewdate'=>'required',
            'maintenance'=>'required',
            'maintenancecharge'=>'required',
            'progress'=>'required',
            'paid'=>'required',
            'dues'=>'required',
            'total'=>'required',
        ]);

        $client = new client;
        $client->name = $request->name;
        $client->slug = $request->slug;
        $client->address = $request->address;
        $client->phone = $request->phone;
        $client->email = $request->email;
        $client->webaddress= $request->webaddress;
        $client->requirement = $request->requirement;
        $client->startdate = $request->startdate;
        $client->renewdate = $request->renewdate;
        $client->maintenance = $request->maintenance;
        $client->maintenancecharge = $request->maintenancecharge;
        $client->progress = $request->progress;
        $client->paid = $request->paid;
        $client->dues = $request->dues;
        $client->total = $request->total;
        $client->save();
        $client->desingations()->sync($request->desingation);
        $client->items()->sync($request->item);

        return redirect(route('client.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.client.view', ['client' => client::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $client = client::with('desingations','items')->where('id',$id)->first();
        $desingations = desingation::all();
        $items = item::all();
        return view('admin.client.edit',compact('desingations','items','client'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request,[

            'name'=>'required',
            'slug'=>'required',
            'address'=>'required',
            'phone'=>'required',
            'webaddress'=>'required',
            'requirement'=>'required',
            'startdate'=>'required',
            'renewdate'=>'required',
            'maintenance'=>'required',
            'maintenancecharge'=>'required',
            'progress'=>'required',
            'paid'=>'required',
            'dues'=>'required',
            'total'=>'required',
        ]);
        $client = client::find($id);
        $client->name = $request->name;
        $client->slug = $request->slug;
        $client->address = $request->address;
        $client->phone = $request->phone;
        $client->email = $request->email;
        $client->webaddress= $request->webaddress;
        $client->requirement = $request->requirement;
        $client->startdate = $request->startdate;
        $client->renewdate = $request->renewdate;
        $client->maintenance = $request->maintenance;
        $client->maintenancecharge = $request->maintenancecharge;
        $client->progress = $request->progress;
        $client->paid = $request->paid;
        $client->dues = $request->dues;
        $client->total = $request->total;
        $client->desingations()->sync($request->desingation);
        $client->items()->sync($request->item);
        $client->save();

        return redirect(route('client.index'))->with('message','Client Update Succecfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        client::where('id',$id)->delete();
        return redirect()->back()->with('message','Client Deleted Succecfully');
    }
}
