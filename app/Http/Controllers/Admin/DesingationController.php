<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\user\desingation;
use App\Http\Controllers\Controller;

class DesingationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {

        $desingations = desingation::all();
        return view('admin.desingation.show',compact('desingations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.desingation.desingation');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[


            'slug'=>'required',

        ]);

        $desingation = new desingation;
        $desingation->slug = $request->slug;
        $desingation->save();


        return redirect(route('desingation.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.desingation.view', ['desingation' => desingation::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $desingation = desingation::where('id',$id)->first();
        return view('admin.desingation.edit',compact('desingation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[

            'slug'=>'required',

        ]);

        $desingation = desingation::find($id);
        $desingation->slug = $request->slug;
        $desingation->save();

        return redirect(route('desingation.index'))->with('message','Desingation Updated Succecfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        desingation::where('id',$id)->delete();
        return redirect()->back()->with('message','Desingation Deleted Succecfully');
    }
}
