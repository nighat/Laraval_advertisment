<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Model\user\invoice;
use App\Model\user\item;
use App\Http\Controllers\Controller;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $invoices = invoice::all();
        return view('admin.invoice.show',compact('invoices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = item::all();
        return view('admin.invoice.invoice',compact('clients','items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request,[
            'name'=>'required',
            'address'=>'required',
            'webaddress'=>'required',
            'invoice_no'=>'required',
            'invoice_date'=>'required',
            'due_date'=>'required',
            'qty'=>'required',
            'price'=>'required',
            'total'=>'required',
        ]);

        $invoice = new invoice;
        $invoice->name= $request->name;
        $invoice->address= $request->address;
        $invoice->webaddress= $request->webaddress;
        $invoice->invoice_no= $request->invoice_no;
        $invoice->invoice_date = $request->invoice_date ;
        $invoice->due_date= $request->due_date ;
        $invoice->qty = $request->qty;
        $invoice->price = $request->price;
        $invoice->total = $request->total;
        $invoice->save();
        $invoice->items()->sync($request->item);

        return redirect(route('invoice.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.invoice.view', ['invoice' => invoice::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $invoice = invoice::with('clients','items')->where('id',$id)->first();
        $items = item::all();
        return view('admin.invoice.edit',compact('clients','items','invoice'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request,[

            'name'=>'required',
            'address'=>'required',
            'webaddress'=>'required',
            'invoice_no'=>'required',
            'invoice_date'=>'required',
            'due_date'=>'required',
            'qty'=>'required',
            'price'=>'required',
            'total'=>'required',
        ]);
        $invoice = invoice::find($id);
        $invoice->name= $request->name;
        $invoice->address= $request->address;
        $invoice->webaddress= $request->webaddress;
        $invoice->invoice_no= $request->invoice_no;
        $invoice->invoice_date = $request->invoice_date ;
        $invoice->due_date= $request->due_date ;
        $invoice->qty = $request->qty;
        $invoice->price = $request->price;
        $invoice->total = $request->total;
        $invoice->items()->sync($request->item);
        $invoice->save();

        return redirect(route('invoice.index'))->with('message','invoice Update Succecfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        invoice::where('id',$id)->delete();
        return redirect()->back()->with('message','invoice Deleted Succecfully');
    }
}
