<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\user\client;

class ClientController extends Controller
{
    public function client(client $client)
    {

        return view('user.client',compact('client'));
    }
}
