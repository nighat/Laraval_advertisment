<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\user\client;
use App\Model\user\desingation;
use App\Model\user\item;


class HomeController extends Controller
{
    public function  index()
    {
        $clients = client::where('status',1)->orderBy('created_at','DESC')->paginate(5);
        return view('user.blog',compact('clients'));
    }
    public function item(item $item)
    {
        $clients = $item->clients();

        return view('user.blog',compact('clients'));
    }
    public function desingation(desingation $desingation)
    {
        $clients = $desingation->clients();

        return view('user.blog',compact('clients'));
    }
}
