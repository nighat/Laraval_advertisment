<?php

namespace App\Model\user;

use Illuminate\Database\Eloquent\Model;

class account extends Model
{

    public function cashs()
    {
        return $this->belongsToMany('App\Model\user\cash','cash_accounts')->withTimestamps();
    }

    public function banknames()
    {
        return $this->belongsToMany('App\Model\user\bankname','account_banknames')->withTimestamps();
    }
    public  function getRouteKeyName()
    {
        return 'slug';
    }
}
