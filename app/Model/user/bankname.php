<?php

namespace App\Model\user;

use Illuminate\Database\Eloquent\Model;

class bankname extends Model
{
    public function accounts()
    {
        return $this->belongsToMany('App\Model\user\account','account_banknames');
    }
}
