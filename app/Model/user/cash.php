<?php

namespace App\Model\user;

use Illuminate\Database\Eloquent\Model;

class cash extends Model
{
    public function accounts()
    {
        return $this->belongsToMany('App\Model\user\account','cash_accounts');
    }
}
