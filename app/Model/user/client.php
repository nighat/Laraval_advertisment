<?php

namespace App\Model\user;

use Illuminate\Database\Eloquent\Model;

class client extends Model
{
    public function items()
    {
        return $this->belongsToMany('App\Model\user\item','item_clients')->withTimestamps();
    }

    public function desingations()
    {
        return $this->belongsToMany('App\Model\user\desingation','client_desingations')->withTimestamps();
    }


}
