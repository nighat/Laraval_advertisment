<?php

namespace App\Model\user;

use Illuminate\Database\Eloquent\Model;

class invoice extends Model
{
    public function items()
    {
        return $this->belongsToMany('App\Model\user\item','item_invoices');
    }

    public function clients()
    {
        return $this->belongsToMany('App\Model\user\client','client_invoices')->withTimestamps();
    }
    public  function getRouteKeyName()
    {
        return 'slug';
    }
}
