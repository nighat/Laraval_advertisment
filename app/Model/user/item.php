<?php

namespace App\Model\user;

use Illuminate\Database\Eloquent\Model;

class item extends Model
{
    public function clients()
    {
        return $this->belongsToMany('App\Model\user\client','item_clients');
    }

    public function invoices()
    {
        return $this->belongsToMany('App\Model\user\invoice','item_invoices');
    }
}
