<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->string('slug',100);
            $table->string('address',250);
            $table->string('phone',250);
            $table->string('email',250);
            $table->string('webaddress',250);
            $table->string('requirement',250);
            $table->string('startdate',250);
            $table->string('renewdate',200);
            $table->string('maintenance',250);
            $table->string('maintenancecharge',250);
            $table->string('progress',250);
            $table->string('paid',250);
            $table->string('dues',250);
            $table->string('total',250);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
