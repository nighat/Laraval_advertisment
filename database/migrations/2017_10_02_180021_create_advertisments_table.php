<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertismentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('org');
            $table->string('address');
            $table->string('phone');
            $table->string('email');
            $table->string('refer');
            $table->string('slide');
            $table->string('duration');
            $table->string('renew');
            $table->string('date');
            $table->string('expense');
            $table->string('type');
            $table->string('amount');
            $table->string('comment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisments');
    }
}
