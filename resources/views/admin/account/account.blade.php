@extends('admin.layouts.app')
@section('main-content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Account Editors
                <small>Advanced form element</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.home') }}"><i class="glyphicon glyphicon-certificate"></i> Home</a></li>
                <li><a href="{{ route('client.index') }}"><i class="glyphicon glyphicon-adjust"></i>Client</a></li>
                <li><a href="{{ route('appoinment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Appoinment</a></li>
                <li><a href="{{ route('advertisment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Advertisment</a></li>
                <li><a href="{{ route('account.index') }}"><i class="glyphicon glyphicon-adjust"></i>Account</a></li>
                <li><a href="{{ route('user.index') }}"><i class="glyphicon glyphicon-adjust"></i>User</a></li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Account Create Form</h3>
                        </div> @include('includes.messages')
                    <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="{{ route('account.store') }}" method="POST">

                            {{ csrf_field() }}
                            <div class="box-body">

                                <div class=" col-lg-offset-3 col-lg-6">

                                    <div class="form-group">
                                        <label for="slug">Name</label>
                                        <input type="text" class="form-control" id="slug"  name="slug" placeholder="Name">
                                    </div>

                                    <div class="form-group">

                                        <label>Cash:</label>

                                        <div class="row">

                                            @foreach($cashs as $cash)

                                                <div class="col-lg-3">
                                                    <div class="checkbox">
                                                        <label> <input type="checkbox" name="cash[]"  value="{{ $cash->id }}">{{ $cash->name }}</label>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>

                                            <div class="form-group">
                                                <label for="advance">Advance</label>
                                                <input type="text" class="form-control" id="advance"  name="advance" placeholder="Advance">
                                            </div>

                                            <div class="form-group">
                                                <label for="dues">Dues</label>
                                                <input type="text" class="form-control" id="dues"  name="dues" placeholder="Dues">
                                            </div>
                                    <div class="form-group">

                                        <label>Bank Name:</label>

                                        <div class="row">

                                            @foreach($banknames as $bankname)

                                                <div class="col-lg-3">
                                                    <div class="checkbox">
                                                        <label> <input type="checkbox" name="bankname[]"  value="{{ $bankname->id }}">{{ $bankname->name }}|{{ $bankname->brance }}</label>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                            <div class="form-group">
                                                <label for="cheque">Cheque No</label>
                                                <input type="text" class="form-control" id="cheque"  name="cheque" placeholder="Cheque No">
                                            </div>

                                            <div class="form-group">
                                                <label for="total">Total</label>
                                                <input type="text" class="form-control" id="total"  name="total" placeholder="Total">
                                            </div>
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <a  href="{{ route('account.index') }}" class="btn btn-warning">Back</a>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                    <!-- /.box -->

                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection