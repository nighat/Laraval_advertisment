@extends('admin.layouts.app')
@section('headSection')

    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables/dataTables.bootstrap.css') }}">

@endsection
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Account
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.home') }}"><i class="glyphicon glyphicon-certificate"></i> Home</a></li>
                <li><a href="{{ route('client.index') }}"><i class="glyphicon glyphicon-adjust"></i>Client</a></li>
                <li><a href="{{ route('appoinment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Appoinment</a></li>
                <li><a href="{{ route('advertisment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Advertisment</a></li>
                <li><a href="{{ route('account.index') }}"><i class="glyphicon glyphicon-adjust"></i>Account</a></li>
                <li><a href="{{ route('user.index') }}"><i class="glyphicon glyphicon-adjust"></i>User</a></li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">


                    <a class='col-lg-offset-5 btn btn-success' href="{{ route('account.create') }}">Add New</a>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="box">
                        <div class="box-header">
                            @include('includes.messages')
                            <h3 class="box-title">Data Table With Full Features</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Name</th>
                                    <th>Cash</th>
                                    <th>Bank Name</th>
                                    <th>Brance Name</th>
                                    <th>Cheque No</th>
                                    <th>Advance</th>
                                    <th>Dues</th>
                                    <th>Total</th>
                                    <th>Created At</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($accounts as $account)

                                    <tr>
                                        <td>{{ $loop->index + 1}}</td>
                                        <td>{{ $account->slug }}</td>
                                        <td>
                                            @foreach($account->cashs as $cash)

                                                {{ $cash->name }} |
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($account->banknames as $bankname)

                                                {{ $bankname->name }} |
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($account->banknames as $bankname)

                                                {{ $bankname->brance }} |
                                            @endforeach
                                        </td>

                                        <td>{{ $account->cheque }}</td>
                                        <td>{{ $account->advance }}</td>
                                        <td>{{ $account->dues }}</td>
                                        <td>{{ $account->advance + $account->dues }}</td>
                                        <td>{{ $account->created_at }}</td>
                                        <td><a href="{{ route('account.edit',$account->id) }}"><span class="glyphicon glyphicon-edit"></span></a></td>
                                        <td>
                                            <form id="delete-form-{{ $account->id }}" method="POST" action="{{ route('account.destroy',$account->id) }}" style="display: none">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                            </form>


                                            <a href="" onclick="
                                                    if(confirm('Are you sure, You went to delete this?'))

                                                    {
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{ $account->id }}').submit();
                                                    }
                                                    else{
                                                    event.preventDefault();
                                                    }
                                                    "><span class="glyphicon glyphicon-trash"></span></a>
                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>S.No</th>
                                    <th>Name</th>
                                    <th>Cash</th>
                                    <th>Bank Name</th>
                                    <th>Brance Name</th>
                                    <th>Cheque No</th>
                                    <th>Advance</th>
                                    <th>Dues</th>
                                    <th>Total</th>
                                    <th>Created At</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div>

                <!-- /.box-footer-->

                <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
@section('footerSection')
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
@endsection