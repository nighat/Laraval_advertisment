@extends('admin.layouts.app')

@section('main-content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Advertisment
                <small>Advanced form element</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">Editors</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Advertisment Create Form</h3>
                        </div>
                    @include('includes.messages')
                    <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="{{ route('advertisment.store') }}" method="POST">

                            {{ csrf_field() }}


                            <div class="box-body">

                                <div class=" col-lg-offset-3 col-lg-6">


                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" id="name"  name="name" placeholder=" Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="org">Organization Name</label>
                                        <input type="text" class="form-control" id="org"  name="org" placeholder="Organization Name">
                                    </div>

                                    <div class="form-group">
                                        <label for="address">Address </label>
                                        <input type="text" class="form-control" id="address"  name="address" placeholder="Address ">
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">Phone </label>
                                        <input type="text" class="form-control" id="phone"  name="phone" placeholder="Phone ">
                                    </div>

                                    <div class="form-group">
                                        <label for="email">E-mail</label>
                                        <input type="text" class="form-control" id="email"  name="email" placeholder="E-mail">
                                    </div>
                                    <div class="form-group">
                                        <label for="refer">Reference</label>
                                        <input type="text" class="form-control" id="refer"  name="refer" placeholder="Reference">
                                    </div>

                                    <div class="form-group">
                                        <label for="slide">Slider No</label>
                                        <input type="number" class="form-control" id="slide"  name="slide" placeholder="Slider No">
                                    </div>

                                    <div class="form-group">
                                        <label for="duration">Duration</label>
                                        <input type="text" class="form-control" id="duration"  name="duration" placeholder="Duration">
                                    </div>

                                    <div class="form-group">
                                        <label for="date">Date</label>
                                        <input type="date" class="form-control" id="date"  name="date" placeholder="Date ">
                                    </div>
                                    <div class="form-group">
                                        <label for="renew">Date</label>
                                        <input type="date" class="form-control" id="renew"  name="renew" placeholder="Renew ">
                                    </div>

                                    <div class="form-group">
                                        <label for="expense">Expense</label>
                                        <input type="text" class="form-control" id="expense"  name="expense" placeholder="Expense">
                                    </div>
                                    <div class="form-group">
                                        <label for="type"> Payment Type</label>
                                        <input type="text" class="form-control" id="type"  name="type" placeholder="Payment Type">
                                    </div>

                                    <div class="form-group">
                                        <label for="amount"> Amount</label>
                                        <input type="number" class="form-control" id="amount"  name="amount" placeholder="Amount">
                                    </div>
                                    <div class="form-group">
                                        <label for="comment"> Comment</label>
                                        <input type="text" class="form-control" id="comment"  name="comment" placeholder="Comment">
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <a  href="{{ route('advertisment.index') }}" class="btn btn-warning">Back</a>
                                    </div>
                                </div>

                            </div>

                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col-->
    </div>
    <!-- ./row -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection