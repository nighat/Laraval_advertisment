@extends('admin.layouts.app')

@section('main-content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Advertisment Editors
                <small>Advanced form element</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.home') }}"><i class="glyphicon glyphicon-certificate"></i> Home</a></li>
                <li><a href="{{ route('client.index') }}"><i class="glyphicon glyphicon-adjust"></i>Client</a></li>
                <li><a href="{{ route('appoinment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Appoinment</a></li>
                <li><a href="{{ route('advertisment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Advertisment</a></li>
                <li><a href="{{ route('account.index') }}"><i class="glyphicon glyphicon-adjust"></i>Account</a></li>
                <li><a href="{{ route('user.index') }}"><i class="glyphicon glyphicon-adjust"></i>User</a></li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Advertisment Edit Form</h3>
                        </div>
                    @include('includes.messages')
                    <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="{{ route('advertisment.update',$advertisment->id) }}" method="POST">

                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <div class="box-body">

                                <div class=" col-lg-offset-3 col-lg-6">


                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" id="name"  name="name" placeholder=" Name" value="{{ $advertisment->name }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="org">Organization Name</label>
                                        <input type="text" class="form-control" id="org"  name="org" placeholder="Organization Name" value="{{ $advertisment->org}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="address">Address </label>
                                        <input type="text" class="form-control" id="address"  name="address" placeholder="Address " value="{{ $advertisment->address}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">Phone </label>
                                        <input type="text" class="form-control" id="phone"  name="phone" placeholder="Phone " value="{{ $advertisment->phone }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="email">E-mail</label>
                                        <input type="text" class="form-control" id="email"  name="email" placeholder="E-mail" value="{{ $advertisment->email }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="refer">Reference</label>
                                        <input type="text" class="form-control" id="refer"  name="refer" placeholder="Reference" value="{{ $advertisment->refer }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="slide">Slider No</label>
                                        <input type="number" class="form-control" id="slide"  name="slide" placeholder="Slider No" value="{{ $advertisment->slide}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="duration">Duration</label>
                                        <input type="text" class="form-control" id="duration"  name="duration" placeholder="Duration" value="{{ $advertisment->duration }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="date">Date</label>
                                        <input type="date" class="form-control" id="date"  name="date" placeholder="Date " value="{{ $advertisment->date }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="renew">Date</label>
                                        <input type="date" class="form-control" id="renew"  name="renew" placeholder="Renew " value="{{ $advertisment->renew }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="expense">Expense</label>
                                        <input type="text" class="form-control" id="expense"  name="expense" placeholder="Expense" value="{{ $advertisment->expense }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="type"> Payment Type</label>
                                        <input type="text" class="form-control" id="type"  name="type" placeholder="Payment Type" value="{{ $advertisment->type }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="amount"> Amount</label>
                                        <input type="number" class="form-control" id="amount"  name="amount" placeholder="Amount" value="{{ $advertisment->amount }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="comment"> Comment</label>
                                        <input type="text" class="form-control" id="comment"  name="comment" placeholder="Comment" value="{{ $advertisment->comment}}">
                                    </div>


                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <a  href="{{ route('advertisment.index') }}" class="btn btn-warning">Back</a>
                                    </div>
                                </div>

                            </div>


                        </form>
                    </div>
                    <!-- /.box -->

                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection