@extends('admin.layouts.app')
@section('headSection')

    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables/dataTables.bootstrap.css') }}">

@endsection


@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Advertisments Information

            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.home') }}"><i class="glyphicon glyphicon-certificate"></i> Home</a></li>
                <li><a href="{{ route('client.index') }}"><i class="glyphicon glyphicon-adjust"></i>Client</a></li>
                <li><a href="{{ route('appoinment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Appoinment</a></li>
                <li><a href="{{ route('advertisment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Advertisment</a></li>
                <li><a href="{{ route('account.index') }}"><i class="glyphicon glyphicon-adjust"></i>Account</a></li>
                <li><a href="{{ route('user.index') }}"><i class="glyphicon glyphicon-adjust"></i>User</a></li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">


                    <a class='col-lg-offset-5 btn btn-success' href="{{ route('advertisment.create') }}">Add New</a>


                </div>
                <div class="box-body">
                    <div class="box">
                        <div class="box-header">
                            @include('includes.messages')
                            <h3 class="box-title">Data Table With Full Features</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Name</th>
                                    <th>Organization</th>
                                    <th>Slide</th>
                                    <th>Durationt</th>
                                    <th>Renew</th>
                                    <th>Date</th>
                                    <th>Expense</th>
                                    <th>Type</th>
                                    <th>Amount</th>
                                    <th>Action</th>


                                </tr>
                                </thead>
                                <tbody>

                                @foreach($advertisments as $advertisment)

                                    <tr>
                                        <td>{{ $loop->index + 1}}</td>
                                        <td>{{ $advertisment->name }}</td>
                                        <td>{{ $advertisment->org }}</td>
                                        <td>{{ $advertisment->slide }}</td>
                                        <td>{{ $advertisment->duration }}</td>
                                        <td>{{ $advertisment->renew}}</td>
                                        <td>{{ $advertisment->date }}</td>
                                        <td>{{ $advertisment->expense }}</td>
                                        <td>{{ $advertisment->type }}</td>
                                        <td>{{ $advertisment->amount }}</td>

                                        <td><div class="btn-group dropdown ">
                                                <button type="button" class="btn btn-instagram dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                    Actions                                     <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu pull-right">


                                                    <li> <a href="{{ route('advertisment.show',$advertisment->id) }}"><i class="fa fa-eye text-success"></i>View</a> </li>

                                                    <li><a href="{{ route('advertisment.edit',$advertisment->id) }}"><span class="glyphicon glyphicon-edit text-success"></span>Edit</a></li>

                                                    <li><form id="delete-form-{{ $advertisment->id }}" method="POST" action="{{ route('advertisment.destroy',$advertisment->id) }}" style="display: none">
                                                            {{ csrf_field() }}
                                                            {{ method_field('DELETE') }}
                                                        </form><a href="" onclick="
                                                                if(confirm('Are you sure, You went to delete this?'))

                                                                {
                                                                event.preventDefault();
                                                                document.getElementById('delete-form-{{ $advertisment->id }}').submit();
                                                                }
                                                                else{
                                                                event.preventDefault();
                                                                }
                                                                "><i class="fa fa-trash-o "></i>Delete</a> </li>
                                                </ul>
                                            </div>
                                        </td>


                                    </tr>
                                @endforeach

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>S.No</th>
                                    <th>Name</th>
                                    <th>Organization</th>
                                    <th>Slide</th>
                                    <th>Durationt</th>
                                    <th>Renew</th>
                                    <th>Date</th>
                                    <th>Expense</th>
                                    <th>Type</th>
                                    <th>Amount</th>
                                    <th>Action</th>



                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection


@section('footerSection')
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
@endsection