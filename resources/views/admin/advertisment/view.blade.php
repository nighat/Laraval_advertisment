@extends('admin.layouts.app')
@section('headSection')

    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables/dataTables.bootstrap.css') }}">

@endsection
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Advertisment
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.home') }}"><i class="glyphicon glyphicon-certificate"></i> Home</a></li>
                <li><a href="{{ route('client.index') }}"><i class="glyphicon glyphicon-adjust"></i>Client</a></li>
                <li><a href="{{ route('appoinment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Appoinment</a></li>
                <li><a href="{{ route('advertisment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Advertisment</a></li>
                <li><a href="{{ route('account.index') }}"><i class="glyphicon glyphicon-adjust"></i>Account</a></li>
                <li><a href="{{ route('user.index') }}"><i class="glyphicon glyphicon-adjust"></i>User</a></li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Details Advertisment Information Data Table</h3>



                    <div class="box-tools pull-right">
                        <a  href="{{ route('advertisment.index') }}" class="btn btn-instagram pull-right">Back</a>
                        <a class='btn btn-instagram pull-right' href="{{ route('advertisment.edit',$advertisment->id) }}"> Edit</a>
                        <a class='btn btn-instagram pull-right' href="{{ route('advertisment.create') }}"> Add</a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="box">
                        <div class="box-header">
                            @include('includes.messages')
                            <h3 class="box-title"></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" >

                            <h4 class="box-title">Advertisment All Data</h4>
                            Name:- {{ $advertisment->name }}<br/>
                            Organization:- {{ $advertisment->org }}<br/>
                            Address:- {{ $advertisment->address }}<br/>
                            Phone:- {{ $advertisment->phone }}<br/>
                            E-mail:- {{ $advertisment->email }}<br/>
                            Refarence:- {{ $advertisment->refer }}<br/>
                            Slide No:- {{ $advertisment->slide }}<br/>
                            Duration:- {{ $advertisment->duration }}<br/>
                            Renew Date:- {{ $advertisment->renew}}<br/>
                            Date:- {{ $advertisment->date }}<br/>
                            Expense:- {{ $advertisment->expense }}<br/>
                            Payments Type:- {{ $advertisment->type }}<br/>
                            Amount:- {{ $advertisment->amount }}<br/>
                            Comments:- {{ $advertisment->comment }}<br/>



                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div>

                <!-- /.box-footer-->

                <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
@section('footerSection')
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
@endsection