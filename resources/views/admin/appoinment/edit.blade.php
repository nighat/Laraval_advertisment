@extends('admin.layouts.app')

@section('main-content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Appoinment Editors
                <small>Advanced form element</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.home') }}"><i class="glyphicon glyphicon-certificate"></i> Home</a></li>
                <li><a href="{{ route('client.index') }}"><i class="glyphicon glyphicon-adjust"></i>Client</a></li>
                <li><a href="{{ route('appoinment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Appoinment</a></li>
                <li><a href="{{ route('advertisment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Advertisment</a></li>
                <li><a href="{{ route('account.index') }}"><i class="glyphicon glyphicon-adjust"></i>Account</a></li>
                <li><a href="{{ route('user.index') }}"><i class="glyphicon glyphicon-adjust"></i>User</a></li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Appoinment Edit Form</h3>
                        </div>
                    @include('includes.messages')
                    <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="{{ route('appoinment.update',$appoinment->id) }}" method="POST">

                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <div class="box-body">

                                <div class=" col-lg-offset-3 col-lg-6">


                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" id="name"  name="name" placeholder=" Name" value="{{ $appoinment->name }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="address">address </label>
                                        <input type="text" class="form-control" id="address"  name="address" placeholder="Address" value="{{ $appoinment->address }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">Phone </label>
                                        <input type="text" class="form-control" id="phone"  name="phone" placeholder="Phone" value="{{ $appoinment->phone }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="visit">Visit</label>
                                        <input type="text" class="form-control" id="visit"  name="visit" placeholder="Visit" value="{{ $appoinment->visit }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="date">Date</label>
                                        <input type="date" class="form-control" id="date"  name="date" placeholder="Date" value="{{ $appoinment->date }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="time">Time</label>
                                        <input type="time" class="form-control" id="time"  name="time" placeholder="Time" value="{{ $appoinment->time }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="purpose">Purpose</label>
                                        <input type="text" class="form-control" id="purpose"  name="purpose" placeholder="Purpose" value="{{ $appoinment->purpose }}">
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <a  href="{{ route('appoinment.index') }}" class="btn btn-warning">Back</a>
                                    </div>
                                </div>

                            </div>


                        </form>
                    </div>
                    <!-- /.box -->

                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection