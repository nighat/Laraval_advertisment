@extends('admin.layouts.app')
@section('headSection')

    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables/dataTables.bootstrap.css') }}">

@endsection
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
             Appoinment
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.home') }}"><i class="glyphicon glyphicon-certificate"></i> Home</a></li>
                <li><a href="{{ route('client.index') }}"><i class="glyphicon glyphicon-adjust"></i>Client</a></li>
                <li><a href="{{ route('appoinment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Appoinment</a></li>
                <li><a href="{{ route('advertisment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Advertisment</a></li>
                <li><a href="{{ route('account.index') }}"><i class="glyphicon glyphicon-adjust"></i>Account</a></li>
                <li><a href="{{ route('user.index') }}"><i class="glyphicon glyphicon-adjust"></i>User</a></li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Details Appoinment Information Data Table</h3>



                    <div class="box-tools pull-right">

                        <a  href="{{ route('appoinment.index') }}" class="btn btn-instagram pull-right">Back</a>
                        <a class='btn btn-instagram pull-right' href="{{ route('appoinment.edit',$appoinment->id) }}"> Edit</a>
                        <a class='btn btn-instagram pull-right' href="{{ route('appoinment.create') }}"> Add</a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="box">
                        <div class="box-header">
                            @include('includes.messages')
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" >

                            <div id="modal-2" class="printableArea">

                                <h3>Appoinment Detail Information</h3>
                            Name:-{{ $appoinment->name }}</br>
                            Address:- {{ $appoinment->address }}<br/>
                            Phone:- {{ $appoinment->phone }}<br/>
                            Visit:- {{ $appoinment->visit }}<br/>
                            Date:- {{ $appoinment->date }}<br/>
                            Time:- {{ $appoinment->time }}<br/>
                            Purpose:- {{ $appoinment->purpose }}<br/><br/><br/>

                                <input type="button" class="printdiv-btn btn btn-instagram " value="Print" />

                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div>

                <!-- /.box-footer-->

                <!-- /.box -->
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
@section('footerSection')
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
    <script>
    $(document).on('click', '.printdiv-btn', function(e) {
    e.preventDefault();

    var $this = $(this);
    var originalContent = $('body').html();
    var printArea = $this.parents('.printableArea').html();

    $('body').html(printArea);
    window.print();
    $('body').html(originalContent);
    });
    </script>
@endsection