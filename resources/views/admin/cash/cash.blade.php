@extends('admin.layouts.app')

@section('main-content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Cash
                <small>Advanced form element</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.home') }}"><i class="glyphicon glyphicon-certificate"></i> Home</a></li>
                <li><a href="{{ route('client.index') }}"><i class="glyphicon glyphicon-adjust"></i>Client</a></li>
                <li><a href="{{ route('appoinment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Appoinment</a></li>
                <li><a href="{{ route('advertisment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Advertisment</a></li>
                <li><a href="{{ route('account.index') }}"><i class="glyphicon glyphicon-adjust"></i>Account</a></li>
                <li><a href="{{ route('user.index') }}"><i class="glyphicon glyphicon-adjust"></i>User</a></li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Cash create form</h3>
                        </div> @include('includes.messages')
                    <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="{{ route('cash.store') }}" method="POST">

                            {{ csrf_field() }}
                            <div class="box-body">

                                <div class=" col-lg-offset-3 col-lg-6">


                                    <div class="form-group">
                                        <label for="name"> Name</label>
                                        <input type="text" class="form-control" id="name"  name="name" placeholder="Item Name">
                                    </div>


                                    <div class="form-group">
                                        <label for="slug">Slug</label>
                                        <input type="text" class="form-control" id="slug"  name="slug" placeholder="Item Slug">
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <a  href="{{ route('cash.index') }}" class="btn btn-warning">Back</a>
                                    </div>
                                </div>

                            </div>


                        </form>
                    </div>
                    <!-- /.box -->

                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection