@extends('admin.layouts.app')
@section('main-content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Client Editors
                <small>Advanced form element</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.home') }}"><i class="glyphicon glyphicon-certificate"></i> Home</a></li>
                <li><a href="{{ route('client.index') }}"><i class="glyphicon glyphicon-adjust"></i>Client</a></li>
                <li><a href="{{ route('appoinment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Appoinment</a></li>
                <li><a href="{{ route('advertisment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Advertisment</a></li>
                <li><a href="{{ route('account.index') }}"><i class="glyphicon glyphicon-adjust"></i>Account</a></li>
                <li><a href="{{ route('user.index') }}"><i class="glyphicon glyphicon-adjust"></i>User</a></li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">  Client Edit Form</h3>
                        </div>
                    @include('includes.messages')
                    <!-- /.box-header -->
                        <!-- form start -->
                        <br/>


                        <form role="form" action="{{ route('client.update',$client->id) }}" method="POST">

                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <div class="box-body">

                                <div class=" col-lg-offset-3  col-lg-6">
                                    <hr/>
                                    <hr/>
                                    <div class="form-group">
                                        <label for="name"> Client Name</label>
                                        <input type="text" class="form-control" id="name"  name="name" placeholder="Name" value="{{ $client->name}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="slug">Organization</label>
                                        <input type="text" class="form-control" id="slug"  name="slug" placeholder="Organization" value="{{ $client->slug}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="address">Address</label>
                                        <input type="text" class="form-control" id="address"  name="address" placeholder="Address" value="{{ $client->address}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">Phone</label>
                                        <input type="text" class="form-control" id="phone"  name="phone" placeholder="Phone Number" value="{{ $client->phone}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="email">E-email</label>
                                        <input type="text" class="form-control" id="email"  name="email" placeholder="E-mail" value="{{ $client->email}}">
                                    </div>

                                    <br/>
                                    <br/>
                                    <br/>
                                    <h4 class="box-title">Business</h4>
                                    <hr/>
                                    <hr/>

                                    <div class="form-group">

                                        <label>Item Name</label>

                                        <div class="row">

                                            @foreach($desingations as $desingation)

                                                <div class="col-lg-3">
                                                    <div class="checkbox">
                                                        <label> <input type="checkbox" name="desingation[]" value="{{ $desingation->id }}"
                                                                       @foreach($client->desingations as $client_desingation)
                                                                       @if($client_desingation->id == $desingation->id)
                                                                       checked
                                                                    @endif
                                                                    @endforeach

                                                            >{{ $desingation->slug }}</label>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="webaddress">Web Address</label>
                                        <input type="text" class="form-control" id="webaddress"  name="webaddress" placeholder="Web Address" value="{{ $client->webaddress}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="requirement">Requirements</label>
                                        <input type="text" class="form-control" id="requirement"  name="requirement" placeholder="Requirements" value="{{ $client->requirement}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="startdate">Start Date</label>
                                        <input type="date" class="form-control" id="startdate"  name="startdate" placeholder="Start Date" value="{{ $client->startdate}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="renewdate">Renew Date</label>
                                        <input type="date" class="form-control" id="renewdate"  name="renewdate" placeholder="Renew Date" value="{{ $client->renewdate}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="maintenance">Maintenance</label>
                                        <input type="text" class="form-control" id="maintenance"  name="maintenance" placeholder="Maintenance" value="{{ $client->maintenance}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="maintenancecharge">Maintenance Charge</label>
                                        <input type="text" class="form-control" id="maintenancecharge"  name="maintenancecharge" placeholder="Maintenance Charge" value="{{ $client->maintenancecharge}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="progress">Progress</label>
                                        <input type="text" class="form-control" id="progress"  name="progress" placeholder="Progress" value="{{ $client->progress}}">
                                    </div>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <h4 class="box-title">Payment</h4>
                                    <hr/>
                                    <hr/>
                                    <div class="form-group">

                                        <label>Payment Type</label>
                                        <div class="row">

                                            @foreach($items as $item)

                                                <div class="col-lg-3">
                                                    <div class="checkbox">
                                                        <label> <input type="checkbox" name="item[]" value="{{ $item->id }}"
                                                                       @foreach($client->items as $client_item)
                                                                       @if($client_item->id == $item->id)
                                                                       checked
                                                                    @endif
                                                                    @endforeach

                                                            >{{ $item->slug}}</label>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="paid">Paid</label>
                                        <input type="text" class="form-control" id="paid"  name="paid" placeholder="Paid" value="{{ $client->paid}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="dues">Dues</label>
                                        <input type="text" class="form-control" id="dues"  name="dues" placeholder="Dues" value="{{ $client->dues}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="total">Total</label>
                                        <input type="text" class="form-control" id="total"  name="total" placeholder="Total" value="{{ $client->total}}">
                                    </div>

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <a  href="{{ route('client.index') }}" class="btn btn-warning">Back</a>
                                    </div>

                                </div>
                                <!-- /.box-body -->



                            </div>

                        </form>

                    </div>
                    <!-- /.box -->

                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection


