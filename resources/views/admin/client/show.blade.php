@extends('admin.layouts.app')
@section('headSection')

    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables/dataTables.bootstrap.css') }}">

@endsection
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Client
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.home') }}"><i class="glyphicon glyphicon-certificate"></i> Home</a></li>
                <li><a href="{{ route('client.index') }}"><i class="glyphicon glyphicon-adjust"></i>Client</a></li>
                <li><a href="{{ route('appoinment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Appoinment</a></li>
                <li><a href="{{ route('advertisment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Advertisment</a></li>
                <li><a href="{{ route('account.index') }}"><i class="glyphicon glyphicon-adjust"></i>Account</a></li>
                <li><a href="{{ route('user.index') }}"><i class="glyphicon glyphicon-adjust"></i>User</a></li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Client Information Data Table</h3>

                    <a class='col-lg-offset-5 btn btn-success' href="{{ route('client.create') }}">Add New</a>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="box">
                        <div class="box-header">
                            @include('includes.messages')
                            <h3 class="box-title">Data Table With Full Features</h3>
                        </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>name</th>
                                <th>Organization</th>
                                <th>Item</th>
                                <th>Progress</th>
                                <th>Maintenance</th>
                                <th>Dues</th>
                                <th>Paid</th>
                                <th>Renew Date</th>
                                <th>Search Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($clients as $client)

                                <tr>
                                    <td>{{ $loop->index + 1}}</td>
                                    <td >{{ $client->name }}</td>
                                    <td>{{ $client->slug }}</td>
                                    <td class="btn btn-success ">@foreach($client->items as $item)

                                            {{ $item->slug }} ||
                                        @endforeach</td>
                                    <td>{{ $client->progress }}</td>
                                    <td>{{ $client->maintenance }}</td>
                                    <td class="btn btn-warning ">{{ $client->dues }}</td>
                                    <td>{{ $client->paid}}</td>
                                    <td>{{ $client->renewdate }}</td>
                                    <td><div class="btn-group dropdown ">
                                            <button type="button" class="btn btn-instagram dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                Actions                                     <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right">



                                                <li><a href="{{ route('client.edit',$client->id) }}"><span class="glyphicon glyphicon-edit"></span>Edit</a></li>
                                                <li><a href="{{ route('client.show',$client->id) }}"><span class="glyphicon glyphicon-eye-open"></span>View</a></li>

                                                <li><form id="delete-form-{{ $client->id }}" method="POST" action="{{ route('client.destroy',$client->id) }}" style="display: none">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                    </form><a href="" onclick="
                                                            if(confirm('Are you sure, You went to delete this?'))

                                                            {
                                                            event.preventDefault();
                                                            document.getElementById('delete-form-{{ $client->id }}').submit();
                                                            }
                                                            else{
                                                            event.preventDefault();
                                                            }
                                                            "><i class="fa fa-trash-o "></i>Delete</a> </li>
                                            </ul>
                                        </div>
                                    </td>

                                </tr>
                            @endforeach

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>S.No</th>
                                <th>name</th>
                                <th>Organization</th>
                                <th>Item</th>
                                <th>Progress</th>
                                <th>Maintenance</th>
                                <th>Dues</th>
                                <th>Paid</th>
                                <th>Renew Date</th>
                                <th>Search Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>

            <!-- /.box-footer-->

    <!-- /.box -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
@section('footerSection')
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
@endsection