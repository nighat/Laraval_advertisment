@extends('admin.layouts.app')
@section('headSection')

    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables/dataTables.bootstrap.css') }}">

@endsection
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                client
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.home') }}"><i class="glyphicon glyphicon-certificate"></i> Home</a></li>
                <li><a href="{{ route('client.index') }}"><i class="glyphicon glyphicon-adjust"></i>client</a></li>
                <li><a href="{{ route('appoinment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Appoinment</a></li>
                <li><a href="{{ route('advertisment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Advertisment</a></li>
                <li><a href="{{ route('account.index') }}"><i class="glyphicon glyphicon-adjust"></i>Account</a></li>
                <li><a href="{{ route('user.index') }}"><i class="glyphicon glyphicon-adjust"></i>User</a></li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Details client Information Data Table</h3>



                    <div class="box-tools pull-right">


                        <a  href="{{ route('client.index') }}" class="btn btn-instagram pull-right">Back</a>
                        <a class='btn btn-instagram pull-right' href="{{ route('client.edit',$client->id) }}"> Edit</a>
                        <a class='btn btn-instagram pull-right' href="{{ route('client.create') }}"> Add</a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="box">
                        <div class="box-header">
                            @include('includes.messages')
                            <h3 class="box-title"></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" >

                            <div id="modal-2" class="printableArea">


                            <h4 class="box-title">client Data</h4>
                            Name:- {{ $client->name }}<br/>
                            Organization Name:- {{ $client->slug }}<br/>
                            Address:- {{ $client->address }}<br/>
                            Phone:- {{ $client->phone }}<br/>
                            E-mail:- {{ $client->email }}<br/>



                            <h4 class="box-title">Business</h4>
                            Item Name:- @foreach($client->items as $item)

                                {{ $item->slug }} ||
                            @endforeach
                            <br/>
                            Web Aaddress:- {{ $client->webaddress }}<br/>
                            Requirement:- {{ $client->requirement }}<br/>
                            Start Date:- {{ $client->startdate }}<br/>
                            Renew Date:- {{ $client->renewdate }}<br/>
                            Maintenance:- {{ $client->maintenance }}<br/>
                            Maintenance Charge:- {{ $client->maintenancecharge }}<br/>
                            Progress:- {{ $client->progress }}<br/>



                            <h4 class="box-title">Payment</h4>
                            Payment Type:@foreach($client->desingations as $desingation)

                                {{ $desingation->slug }} ||
                            @endforeach<br/>
                            Paid:- {{ $client->paid }}<br/>
                            Dues:- {{ $client->dues }}<br/>
                            Total:- {{ $client->paid + $client->dues }}<br/>
                            <br/>
                           <br/>
                                <input type="button" class="printdiv-btn btn btn-instagram " value="Print" />

                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div>

                <!-- /.box-footer-->
            </div>
                <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
@section('footerSection')
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
    <script>
        $(document).on('click', '.printdiv-btn', function(e) {
            e.preventDefault();

            var $this = $(this);
            var originalContent = $('body').html();
            var printArea = $this.parents('.printableArea').html();

            $('body').html(printArea);
            window.print();
            $('body').html(originalContent);
        });
    </script>
@endsection