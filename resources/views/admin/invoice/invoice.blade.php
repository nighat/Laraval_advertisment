@extends('admin.layouts.app')
@section('headSection')

    <link rel="stylesheet" href="{{ asset('admin/plugins/select2/select2.min.css') }}">

@endsection
@section('main-content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Invoice
                <small>Advanced form element</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.home') }}"><i class="glyphicon glyphicon-certificate"></i> Home</a></li>
                <li><a href="{{ route('client.index') }}"><i class="glyphicon glyphicon-adjust"></i>Client</a></li>
                <li><a href="{{ route('appoinment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Appoinment</a></li>
                <li><a href="{{ route('advertisment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Advertisment</a></li>
                <li><a href="{{ route('account.index') }}"><i class="glyphicon glyphicon-adjust"></i>Account</a></li>
                <li><a href="{{ route('invoice.index') }}"><i class="glyphicon glyphicon-adjust"></i>Invoice</a></li>
                <li><a href="{{ route('user.index') }}"><i class="glyphicon glyphicon-adjust"></i>User</a></li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Invoice Create Form</h3>
                        </div>
                    @include('includes.messages')
                    <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="{{ route('invoice.store') }}" method="POST">

                            {{ csrf_field() }}


                            <div class="box-body">

                                <div class=" col-lg-4">

                                 <div class="form-group">
                                        <label for="invoice_no">Invoice No</label>
                                        <input type="text" class="form-control" id="invoice_no"  name="invoice_no" placeholder=" Invoice No">
                                    </div>

                                    <div class="form-group">
                                        <label for="address">Client Address</label>
                                        <input type="text" class="form-control" id="address"  name="address" placeholder=" Client Address">
                                    </div>
                                </div>


                                <div class=" col-lg-4">

                                    <div class="form-group">
                                        <label for="name">Client Name</label>
                                        <input type="text" class="form-control" id="name"  name="name" placeholder=" Client Name">
                                    </div>

                                    <div class="form-group">
                                        <label for="webaddress">Web Address</label>
                                        <input type="text" class="form-control" id="webaddress"  name="webaddress" placeholder=" Web Address">
                                    </div>


                                </div>


                                <div class=" col-lg-4">

                                    <div class="form-group">
                                        <label for="invoice_date">Invoice Date </label>
                                        <input type="date" class="form-control" id="invoice_date"  name="invoice_date" placeholder="Invoice Date">
                                    </div>

                                    <div class="form-group">
                                        <label for="due_date">Due Date </label>
                                        <input type="date" class="form-control" id="due_date"  name="due_date" placeholder="Due Date">
                                    </div>
                                </div>

                                <div class=" col-lg-10">
                                    <label>Item Name</label>

                                    <div class="row">

                                        @foreach($items as $item)

                                            <div class="col-lg-3">
                                                <div class="checkbox">
                                                    <label> <input type="checkbox" name="item[]" value="{{ $item->id }}">{{ $item->slug }}</label>
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>

                            </div>

                                <div class=" col-lg-3">
                                    <div class="form-group">
                                        <label for="qty">Quantity</label>
                                        <input type="number" class="form-control" id="qty"  name="qty" placeholder="Quantity ">
                                    </div>
                                </div>

                                <div class=" col-lg-3">
                                        <div class="form-group">
                                            <label for="price">Price</label>
                                            <input type="number" class="form-control" id="price"  name="price" placeholder="Price ">
                                        </div>
                                </div>
                                <div class=" col-lg-3">
                                    <div class="form-group">
                                        <label for="total">Total</label>
                                        <input type="number" class="form-control" id="total"  name="total" placeholder="Total ">
                                    </div>
                                </div>
                                <div class=" col-lg-5">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a  href="{{ route('invoice.index') }}" class="btn btn-warning">Back</a>
                            </div>
                                </div>
                    </div>





                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col-->
            </div>
    <!-- ./row -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
@section('footerSection')

    <script src="{{ asset('admin/plugins/select2/select2.full.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            //Initialize Select2 Elements
            $(".select2").select2();
        });

    </script>

@endsection