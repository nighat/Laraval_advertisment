@extends('admin.layouts.app')
@section('headSection')

    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables/dataTables.bootstrap.css') }}">

@endsection
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
             invoice
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.home') }}"><i class="glyphicon glyphicon-certificate"></i> Home</a></li>
                <li><a href="{{ route('client.index') }}"><i class="glyphicon glyphicon-adjust"></i>Client</a></li>
                <li><a href="{{ route('appoinment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Appoinment</a></li>
                <li><a href="{{ route('advertisment.index') }}"><i class="glyphicon glyphicon-adjust"></i>Advertisment</a></li>
                <li><a href="{{ route('account.index') }}"><i class="glyphicon glyphicon-adjust"></i>Account</a></li>
                <li><a href="{{ route('invoice.index') }}"><i class="glyphicon glyphicon-adjust"></i>Invoice</a></li>
                <li><a href="{{ route('user.index') }}"><i class="glyphicon glyphicon-adjust"></i>User</a></li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Details Invoice Information Data Table</h3>



                    <div class="box-tools pull-right">

                        <a  href="{{ route('invoice.index') }}" class="btn btn-instagram pull-right">Back</a>
                        <a class='btn btn-instagram pull-right' href="{{ route('invoice.edit',$invoice->id) }}"> Edit</a>
                        <a class='btn btn-instagram pull-right' href="{{ route('invoice.create') }}"> Add</a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="box">
                        <div class="box-header">
                            @include('includes.messages')
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" >

                            <div id="modal-2" class="printableArea">

                                <h3>Client Invoice  <p class="pull-right">{{ $invoice->invoice_date }}</p></h3>
                            <hr/> <br/>
                             <table width="100%">
                                 <tr>
                                     <td><b>From</b></td>
                                     <td><b>To</b></td>
                                     <td><b>Invoice No:</b> {{ $invoice->invoice_no }}</td>
                                 </tr>
                                 <tr>
                                     <td>Admin, Inc.</td>
                                     <td>{{ $invoice->name}}</td>
                                     <td><b>Invoice Date:</b> {{ $invoice->invoice_date }}</td></tr>
                                 <tr>
                                 <tr>
                                     <td> 795 Folsom Ave, Suite 600 San Francisco, CA 94107</td>
                                     <td>{{ $invoice->address}}</td>
                                     <td><b>Due Date:</b> {{ $invoice->due_date}}</td></tr>
                                 <tr>
                                 <tr>
                                     <td>  Email: info@almasaeedstudio.com</td>
                                     <td>{{ $invoice->webaddress}}</td>
                                     <td></td></tr>
                                 <tr>

                             </table>
                                <hr/>

                                <table width="90%" border="2px">
                                    <thead>
                                    <tr>

                                        <th style=" padding: 20px;text-align:center;">Invoice No</th>
                                        <th style=" padding: 20px;text-align: center;">Product Name</th>
                                        <th style=" padding: 20px;text-align: center;">Quantity</th>
                                        <th style=" padding: 20px;text-align: center;">Price</th>
                                        <th style=" padding: 20px;text-align: center;">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>

                                        <td style=" padding: 20px;text-align: center;">{{ $invoice->invoice_no }}</td>
                                        <td style=" padding: 20px;text-align: center;">@foreach($invoice->items as $item)

                                                {{ $item->slug }}
                                            @endforeach</td>
                                        <td style=" padding: 20px;text-align: center;">{{ $invoice->qty }}</td>
                                        <td style=" padding: 20px; text-align: center;">{{ $invoice->price }}</td>
                                        <td style=" padding: 20px; text-align: center;">{{ $invoice->qty * $invoice->price }}</td>
                                    </tr>
                                    </tbody>

                            </table>

                                <br/><br/>

                                <input type="button" class="printdiv-btn btn btn-instagram pad margin no-print" value="Print" />


                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div>

                <!-- /.box-footer-->

                <!-- /.box -->
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
@section('footerSection')
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
    <script>
    $(document).on('click', '.printdiv-btn', function(e) {
    e.preventDefault();

    var $this = $(this);
    var originalContent = $('body').html();
    var printArea = $this.parents('.printableArea').html();

    $('body').html(printArea);
    window.print();
    $('body').html(originalContent);
    });
    </script>
@endsection