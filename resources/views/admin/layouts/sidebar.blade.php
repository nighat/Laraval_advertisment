<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>

            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->

        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Clients Information</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="{{ route('client.index') }}"><i class="fa fa-circle-o"></i>Clients</a></li>
                    <li class="active"><a href="{{ route('item.index') }}"><i class="fa fa-circle-o"></i>Item </a></li>
                    <li class="active"><a href="{{ route('desingation.index') }}"><i class="fa fa-circle-o"></i>Payment Type </a></li>
                    <li class="active"><a href="{{ route('appoinment.index') }}"><i class="fa fa-circle-o"></i>Appoinment</a></li>
                    <li class="active"><a href="{{ route('advertisment.index') }}"><i class="fa fa-circle-o"></i>Advertisment</a></li>
                    <li class="active"><a href="{{ route('invoice.index') }}"><i class="fa fa-circle-o"></i>Invoice</a></li>

                </ul>
            </li>

            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Account Information</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="{{ route('account.index') }}"><i class="fa fa-circle-o"></i> Accounts</a></li>
                    <li class="active"><a href="{{ route('cash.index') }}"><i class="fa fa-circle-o"></i>Cash</a></li>
                    <li class="active"><a href="{{ route('bankname.index') }}"><i class="fa fa-circle-o"></i>Bank Name</a></li>

                </ul>
            </li>
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>User</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="{{ route('user.index') }}"><i class="fa fa-circle-o"></i> User</a></li>
                    <li class="active"><a href="{{ route('role.index') }}"><i class="fa fa-circle-o"></i>Role</a></li>
                    <li class="active"><a href="{{ route('permission.index') }}"><i class="fa fa-circle-o"></i>Permission</a></li>

                </ul>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>