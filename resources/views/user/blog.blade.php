@extends('user/app')

@section('bg-img',asset('user/img/home-bg.jpg'))
@section('title','Bitfumes Blog')
@section('sub-heading','Learn Together and Grow Together')

 @section('main-content')
        <!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
            @foreach( $clients as $client)

                <div class="client-preview">

                    <a href="{{ route('client',$client->slug) }}">
                        <h2 class="client-title">
                            {{ $client->name}}
                        </h2>
                        <h3 class="client-subtitle">
                            {{ $client->org }}
                        </h3>
                    </a>
                    <p class="client-meta">cliented by <a href="#">Start Bootstrap</a> {{ $client->created_at->diffForHumans() }}</p>
                </div>
            @endforeach
            <hr>
            <!-- Pager -->
            <ul class="pager">
                <li class="next">
                    {{ $clients->links() }}

                </li>
            </ul>
        </div>
    </div>
</div>
      @endsection
