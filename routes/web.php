<?php

///User Routs

Route::group(['namespace' => 'User'],function (){

    Route::get('/', 'HomeController@index');

    Route::get('client/{client}','clientController@client')->name('client');


    Route::get('client/item/{item}','HomeController@item')->name('item');
    Route::get('client/desingation/{desingation}','HomeController@desingation')->name('desingation');

    Route::get('account', 'accountController@index')->name('account');
    Route::get('appoinment', 'appoinmentController@index')->name('appoinment');
});


//Admin Routes

Route::group(['namespace' => 'Admin'],function (){

    Route::get('admin/home', 'HomeController@index')->name('admin.home');
    //User Routes
    Route::resource('admin/user', 'UserController');

//Roles Routes
    Route::resource('admin/role', 'RoleController');

    //Permission Routes
    Route::resource('admin/permission', 'PermissionController');

    //Client Routes
    Route::resource('admin/client', 'ClientController');

    //item Routes
    Route::resource('admin/item', 'itemController');

    //desingation Routes
    Route::resource('admin/desingation', 'desingationController');

    //Account Routes
    Route::resource('admin/account', 'accountController');

    //Cash Routes
    Route::resource('admin/cash', 'cashController');

    //Bankname Routes
    Route::resource('admin/bankname', 'banknameController');

   //Appoinment Routes
    Route::resource('admin/appoinment', 'appoinmentController');

    Route::resource('admin/advertisment', 'advertismentController');

    Route::resource('admin/invoice', 'InvoiceController');

// Admin Auth Routes
    Route::get('admin-login','Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('admin-login','Auth\LoginController@login');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');